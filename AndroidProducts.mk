#
# Copyright (C) 2020 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/evolution_griffin.mk

COMMON_LUNCH_CHOICES := \
    evolution_griffin-user \
    evolution_griffin-userdebug \
    evolution_griffin-eng
